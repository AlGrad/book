package com.example.Book.Project.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.apache.tomcat.util.http.parser.Authorization;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.Book.Project.DTO.AuthorDTO;
import com.example.Book.Project.DTO.PaperDTO;
import com.example.Book.Project.DTO.PublisherDTO;
import com.example.Book.Project.exceptions.ResourceNotFoundException;
import com.example.Book.Project.models.Author;
import com.example.Book.Project.models.Paper;
import com.example.Book.Project.models.Publisher;
import com.example.Book.Project.repositories.AuthorRepositories;


@RestController
@RequestMapping("/api")
public class AuthorController {
	ModelMapper modelMapper = new ModelMapper();
	
	@GetMapping("/authors/readAll")
	public HashMap<String,Object>getAllPublisherDTOMapper(){
		HashMap<String, Object>result = new HashMap<String, Object>();
		
		//Mengambil semua data dari publisher dengan menggunakan method findAll() dari repository
		ArrayList<Author>listAuthorEntity = (ArrayList<Author>) authorRepository.findAll();
		
		
		//Membuat sebuah array list DTO
		ArrayList<AuthorDTO>listAuthorDTO = new ArrayList<AuthorDTO>();
		
		//mapping semua object dari entity ke DTO menggunakan looping 
		
		for(Author author : listAuthorEntity) {
			//inisialisasi obj Publisher DTO 
			AuthorDTO authorDTO = modelMapper.map(author, AuthorDTO.class);

			
			listAuthorDTO.add(authorDTO);
			
		}
		
		
		result.put("Status", 200);
		result.put("Message", "Read All author Mapp success");
		result.put("Data", listAuthorDTO);
		
		return result;
	}
	
	@Autowired
	AuthorRepositories authorRepository;
	
	

	
	// CREATE MAPPER
	
	
	
	@PostMapping("/authors/creates")
	public HashMap<String, Object> createAuthorDTOMapper(@Valid @RequestBody AuthorDTO body){
		HashMap<String , Object> result = new HashMap<String, Object>();
		
	
		
		Author authorEntity = modelMapper.map(body, Author.class);
		//proses saving data ke database
	
		
		authorRepository.save(authorEntity);
		
		body.setAuthorId(authorEntity.getAuthorId());
		
		
		//Mapping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Create new authorDTO Mapp success");
		result.put("Data", body);
		
		
		return result;
	}

	
	//UPDATE MAPPER
	@PutMapping("/authors/update/{id}")
	public HashMap<String, Object> updateAuthorDTOMapper(@PathVariable(value = "id") Long authorId, @Valid @RequestBody AuthorDTO body){
		HashMap<String , Object> result = new HashMap<String, Object>();
		
		Author authorEntity = authorRepository.findById(authorId).orElseThrow(()-> new ResourceNotFoundException("Author", "id", authorId));
		
		
		authorEntity = modelMapper.map(body, Author.class);
		
		authorEntity.setAuthorId(authorId);
	
		
		authorRepository.save(authorEntity);
		
		body = modelMapper.map(authorEntity, AuthorDTO.class);
		
		
		//Mapping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Update new authorDTO Mapp success");
		result.put("Data", body);
		
		
		return result;
	}
	
	
	
	@DeleteMapping("/authors/delete/{id}")
	public HashMap<String, Object>deleteAuthorDTOMapper(@PathVariable(value = "id")Long authorId,  AuthorDTO body){
		HashMap<String , Object> result = new HashMap<String, Object>();
		
		
		Author authorEntity = authorRepository.findById(authorId)
				.orElseThrow(()-> new ResourceNotFoundException("Author", "id", authorId));
		
		authorEntity = modelMapper.map(body, Author.class);
		authorEntity.setAuthorId(authorId);;
		
		authorRepository.delete(authorEntity);
		
		
		result.put("Status", 200);
		result.put("Message", "Delete authorDTO Mapp success");
		result.put("Data", "[]");
		
		return result;
		
	}

}




//Cat:


////@GetMapping("/authors/readAllDTO")
////public HashMap<String,Object>getAllData(){
////HashMap<String, Object>result = new HashMap<String, Object>();
////
//////Mengambil semua data dari author dengan menggunakan method findAll() dari repository
////ArrayList<Author>listAuthorEntity = (ArrayList<Author>) authorRepository.findAll();
////
////
//////Membuat sebuah array list DTO
////ArrayList<AuthorDTO>listAuthorDTO = new ArrayList<AuthorDTO>();
////
//////mapping semua object dari entity ke DTO menggunakan looping 
////
////for(Author author : listAuthorEntity) {
////	//inisialisasi obj Publisher DTO 
////	AuthorDTO authorDTO = new AuthorDTO();
////	
////	//Mapping nilai dari entity ke DTO
////	authorDTO.setAge(author.getAge());
////	authorDTO.setCountry(author.getCountry());
////	authorDTO.setAuthorId(author.getAuthorId());
////	authorDTO.setFirstName(author.getFirstName());
////	authorDTO.setGender(author.getGender());
////	authorDTO.setLastName(author.getLastName());
////	authorDTO.setRating(author.getRating());
////
////	
////	listAuthorDTO.add(authorDTO);
////	
////}
////
////
////result.put("Status", 200);
////result.put("Message", "Read all authorDTO success");
////result.put("Data", listAuthorDTO);
////
////return result;
////}
//
//@PostMapping("/authors/creates")
//public Author createAuthor(@Valid @RequestBody Author author){
//return authorRepository.save(author);
//}

//@PostMapping("/author/createsDTO")
//public HashMap<String, Object> createAuthorDTO(@Valid @RequestBody AuthorDTO body){
//HashMap<String , Object> result = new HashMap<String, Object>();
//
////buat sebuah object paper entity
//Author authorEntity =new Author();
//authorEntity.setAge(body.getAge());
//authorEntity.setCountry(body.getCountry());
//authorEntity.setFirstName(body.getFirstName());
//authorEntity.setLastName(body.getLastName());
//authorEntity.setGender(body.getGender());
//authorEntity.setRating(body.getRating());
//
////proses saving data ke database
//authorRepository.save(authorEntity);
//
//body.setAuthorId(authorEntity.getAuthorId());
//
//
////Mapping result untuk response API
//result.put("Status", 200);
//result.put("Message", "Create new author success");
//result.put("Data", body);
//
//
//return result;
//}


//@DeleteMapping("/author/deleteDTO/{id}")
//public HashMap<String, Object>deleteAuthorDTO(@PathVariable(value = "id")Long authorId, AuthorDTO body){
//	HashMap<String , Object> result = new HashMap<String, Object>();
//	
//	
//	Author authorEntity = authorRepository.findById(authorId)
//			.orElseThrow(()-> new ResourceNotFoundException("Author", "id", authorId));
//	
//	
//	//buat sebuah object paper entity
//	authorEntity.setAge(body.getAge());
//	authorEntity.setCountry(body.getCountry());
//	authorEntity.setFirstName(body.getFirstName());
//	authorEntity.setLastName(body.getLastName());
//	authorEntity.setGender(body.getGender());
//	authorEntity.setRating(body.getRating());
//	
//	
//	authorRepository.delete(authorEntity);
//	
//	
//	
//	result.put("Status", 200);
//	result.put("Message", "Delete author success");
//	result.put("Data", body);
//	
//	return result;
//	
//}


//@PutMapping("/author/updateDTO/{id}")
//public HashMap<String, Object> updateAuthorrDTO(@PathVariable(value = "id") Long authorId, @Valid @RequestBody AuthorDTO body){
//	HashMap<String , Object> result = new HashMap<String, Object>();
//	
//	
//	Author authorEntity = authorRepository.findById(authorId).orElseThrow(()-> new ResourceNotFoundException("Author", "id", authorId));
//	
//	authorEntity.setAge(body.getAge());
//	authorEntity.setCountry(body.getCountry());
//	authorEntity.setFirstName(body.getFirstName());
//	authorEntity.setLastName(body.getLastName());
//	authorEntity.setGender(body.getGender());
//	authorEntity.setRating(body.getRating());
//	
//	//proses saving data ke database
//	authorRepository.save(authorEntity);
//	
//	body.setAuthorId(authorEntity.getAuthorId());
//	
//	//Mapping result untuk response API
//	result.put("Status", 200);
//	result.put("Message", "update author success");
//	result.put("Data", body);
//	
//	
//	
//	return result;
//}

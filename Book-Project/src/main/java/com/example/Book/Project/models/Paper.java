package com.example.Book.Project.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PositiveOrZero;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity(name = "PaperQuality")
@Table(name = "Paper_Quality")
//@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value =  {"paperid"},
		allowSetters = true)
public class Paper implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "paperid")
	private Long paperid;
	
	@Column(nullable = false)
	private String qualityName;
	
	@PositiveOrZero
	private BigDecimal paperPrice;
	
	
	 @OneToMany(
		        mappedBy = "paperQuality",
		        cascade = CascadeType.PERSIST,
		        fetch =  FetchType.LAZY
		    )
	private Set<Publisher> publishers;
	
	public Paper() {
		publishers = new HashSet<>();
	}
	
	public Paper(String qualityName, BigDecimal paperPrice) {
		super();
		publishers = new HashSet<>() ;
		this.qualityName = qualityName;
		this.paperPrice = paperPrice;
		
	}

	public Long getPaperid() {
		return paperid;
	}

	public void setPaperid(Long paperid) {
		this.paperid = paperid;
	}

	public String getQualityName() {
		return qualityName;
	}

	public void setQualityName(String qualityName) {
		this.qualityName = qualityName;
	}

	public BigDecimal getPaperPrice() {
		return paperPrice;
	}

	public void setPaperPrice(BigDecimal paperPrice) {
		this.paperPrice = paperPrice;
	}
		 
	public Set<Publisher> getPublishers() {
        return publishers;
    }
	
	public void setPublishers(Set<Publisher> publishers) {
        this.publishers = publishers;
        for (Publisher publisherList : publishers) {
        	publisherList.setPaperQuality(this);
        }
    }

}

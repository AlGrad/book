package com.example.Book.Project.controllers;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.Book.Project.DTO.BookDTO;
import com.example.Book.Project.DTO.RatingDTO;
import com.example.Book.Project.exceptions.ResourceNotFoundException;
import com.example.Book.Project.models.Book;
import com.example.Book.Project.models.Rating;
import com.example.Book.Project.repositories.RatingRepositories;



@RestController
@RequestMapping("/api")
public class RatingController {
	
	ModelMapper modelMapper = new ModelMapper();
	
	@Autowired
	RatingRepositories ratingRepositories;
	
	@GetMapping("/ratings/readAll")
	public HashMap<String,Object>getAllRatingDTOMapper(){
		HashMap<String, Object>result = new HashMap<String, Object>();
		
		//Mengambil semua data dari publisher dengan menggunakan method findAll() dari repository
		ArrayList<Rating>listRatingEntity = (ArrayList<Rating>) ratingRepositories.findAll();
		
		
		//Membuat sebuah array list DTO
		ArrayList<RatingDTO>listRatingDTO = new ArrayList<RatingDTO>();
		
		//mapping semua object dari entity ke DTO menggunakan looping 
		
		for(Rating rating : listRatingEntity) {
			//inisialisasi obj Publisher DTO 
			RatingDTO ratingDTO = modelMapper.map(rating, RatingDTO.class);

			
			listRatingDTO.add(ratingDTO);
			
		}
		
		
		result.put("Status", 200);
		result.put("Message", "Read all rating success");
		result.put("Data", listRatingDTO);
		
		return result;
	}
	
	@PostMapping("/rating/creates")
	public HashMap<String, Object> createRatingDTOMapper(@Valid @RequestBody RatingDTO body){
		HashMap<String , Object> result = new HashMap<String, Object>();
		
	
		
		Rating ratingEntity = modelMapper.map(body, Rating.class);
		//proses saving data ke database
	
		
		ratingRepositories.save(ratingEntity);
		
		body.setRatingID(ratingEntity.getRatingID());
		
		
		//Mapping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Create new rating success");
		result.put("Data", body);
		
		
		return result;
	}
	

	@PutMapping("/rating/update/{id}")
	public HashMap<String, Object> updateRatingDTOMapper(@PathVariable(value = "id") Long ratingID, @Valid @RequestBody RatingDTO body){
		HashMap<String , Object> result = new HashMap<String, Object>();
		
		Rating ratingEntity = ratingRepositories.findById(ratingID).orElseThrow(()-> new ResourceNotFoundException("Rating", "id", ratingID));
		
		
		ratingEntity = modelMapper.map(body, Rating.class);
		
		ratingEntity.setRatingID(ratingID);
		
		ratingRepositories.save(ratingEntity);
		
		body = modelMapper.map(ratingEntity, RatingDTO.class);
		
		
		//Mapping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Update new rating success");
		result.put("Data", body);
		
		
		return result;
	}
	
	
	@DeleteMapping("/rating/delete/{id}")
	public HashMap<String, Object>deleteRatingDTOMapper(@PathVariable(value = "id")Long ratingID,  RatingDTO body){
		HashMap<String , Object> result = new HashMap<String, Object>();
		
		
		Rating ratingEntity = ratingRepositories.findById(ratingID)
				.orElseThrow(()-> new ResourceNotFoundException("Rating", "id", ratingID));
		
		ratingEntity = modelMapper.map(body, Rating.class);
		ratingEntity.setRatingID(ratingID);
		
		ratingRepositories.delete(ratingEntity);
		
		
		result.put("Status", 200);
		result.put("Message", "Delete bookDTO Mapp success");
		result.put("Data", "[]");
		
		return result;
		
	}

}

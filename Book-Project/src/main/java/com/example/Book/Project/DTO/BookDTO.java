package com.example.Book.Project.DTO;

import java.math.BigDecimal;
import java.util.Date;

import com.example.Book.Project.models.Author;
import com.example.Book.Project.models.Publisher;

public class BookDTO {
	private Long bookId;
	private String title;
	private Date releaseDate;
	private AuthorDTO authorMap;
	private PublisherDTO publisherMap;
	private BigDecimal price;
	
	public BookDTO() {
		super();
	}

	public BookDTO(Long bookId, String title, Date releaseDate, AuthorDTO authorMap, PublisherDTO publisherMap,
			BigDecimal price) {
		super();
		this.bookId = bookId;
		this.title = title;
		this.releaseDate = releaseDate;
		this.authorMap = authorMap;
		this.publisherMap = publisherMap;
		this.price = price;
	}

	public Long getBookId() {
		return bookId;
	}

	public void setBookId(Long bookId) {
		this.bookId = bookId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	public AuthorDTO getAuthorMap() {
		return authorMap;
	}

	public void setAuthorMap(AuthorDTO authorMap) {
		this.authorMap = authorMap;
	}

	public PublisherDTO getPublisherMap() {
		return publisherMap;
	}

	public void setPublisherMap(PublisherDTO publisherMap) {
		this.publisherMap = publisherMap;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	
	
	

}

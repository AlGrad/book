package com.example.Book.Project.models;



import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "Publisher")
//@JsonIgnoreProperties(value = {"createdAt", "updatedAt"},
//	allowGetters = true)
public class Publisher implements Serializable{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "publisherID")
	private Long publisherID;
	
	@Column(nullable = false)
	private String companyName;
	
	@Column(nullable = false)
	private String country;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "paperID")
	private Paper paperQuality;
	
	
	@OneToMany(
	        mappedBy = "publisherMap",
	        cascade = CascadeType.PERSIST,
	        fetch =  FetchType.LAZY
		    )
	private Set<Book>books;
	
	public Publisher() {
		super();
		books = new HashSet<>();
	}
	
	

	public Publisher(String companyName, String country) {
		super();
		books = new HashSet<>();
		this.companyName = companyName;
		this.country = country;
	}

	public Long getPublisherID() {
		return publisherID;
	}

	public void setPublisherID(Long publisherID) {
		this.publisherID = publisherID;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Paper getPaperQuality() {
		return paperQuality;
	}

	public void setPaperQuality(Paper paperQuality) {
		this.paperQuality = paperQuality;
		paperQuality.getPublishers().add(this);
	}

	public Set<Book> getBooks() {
		return books;
	}

	public void setBooks(Set<Book> books) {
		this.books = books;
		 for (Book bookList : books) {
	        	bookList.setPublisherMap(this);
	        }
	}
	
	
}

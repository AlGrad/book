package com.example.Book.Project.DTO;

import java.math.BigDecimal;

public class PaperDTO {
	private Long paperid;
	private String qualityName;
	private BigDecimal paperPrice;
	
	
	public PaperDTO() {
		super();
	}
	public PaperDTO(Long paperid, String qualityName, BigDecimal paperPrice) {
		super();
		this.paperid = paperid;
		this.qualityName = qualityName;
		this.paperPrice = paperPrice;
	}
	public Long getPaperid() {
		return paperid;
	}
	public void setPaperid(Long paperid) {
		this.paperid = paperid;
	}
	public String getQualityName() {
		return qualityName;
	}
	public void setQualityName(String qualityName) {
		this.qualityName = qualityName;
	}
	public BigDecimal getPaperPrice() {
		return paperPrice;
	}
	public void setPaperPrice(BigDecimal paperPrice) {
		this.paperPrice = paperPrice;
	}
	
	
	

}

package com.example.Book.Project.models;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Positive;

import com.example.Book.Project.repositories.AuthorRepositories;

@Entity(name = "Author")
@Table(name = "Authors")
public class Author implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "authorID", nullable = false, unique = true)
	private Long authorId;
	
	@Column(name = "author_fname", nullable = false)
	private String firstName;
	
	@Column(name = "author_lname", nullable = false)
	private String lastName;
	
	@Column(name = "gender", nullable = false)
	private String gender;
	
	@Column(name = "age")
	@Positive
	private int age;
	
	@Column(name = "country", nullable = false)
	private String country;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ratingId")
	private Rating ratingAuthor;
	
	 @OneToMany(
		        mappedBy = "authorMap",
		        cascade = CascadeType.PERSIST,
		        fetch =  FetchType.LAZY
		    )
	private Set<Book>books;
	
	
	public Author() {
		super();
		books = new HashSet<>();
	}

	
	public Author(String firstName, String lastName, String gender, @Positive int age, String country, Rating ratingAuthor) {
		super();
		books = new HashSet<>();
		this.firstName = firstName;
		this.lastName = lastName;
		this.gender = gender;
		this.age = age;
		this.country = country;
		this.ratingAuthor = ratingAuthor;
	}


	public Long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	

	public Rating getRatingAuthor() {
		return ratingAuthor;
	}


	public void setRatingAuthor(Rating ratingAuthor) {
		this.ratingAuthor = ratingAuthor;
		ratingAuthor.getAuthors().add(this);
	}


	public Set<Book> getBooks() {
		return books;
	}

	public void setBooks(Set<Book> books) {
		this.books = books;
		 for (Book bookList : books) {
	        	bookList.setAuthorMap(this);
	        }
	}
	
	
	

}

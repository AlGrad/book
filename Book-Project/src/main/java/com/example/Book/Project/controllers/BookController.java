package com.example.Book.Project.controllers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.Book.Project.DTO.AuthorDTO;
import com.example.Book.Project.DTO.BookDTO;
import com.example.Book.Project.DTO.PaperDTO;
import com.example.Book.Project.DTO.PublisherDTO;
import com.example.Book.Project.exceptions.ResourceNotFoundException;
import com.example.Book.Project.models.Author;
import com.example.Book.Project.models.Book;
import com.example.Book.Project.models.Paper;
import com.example.Book.Project.models.Publisher;
import com.example.Book.Project.models.Rating;
import com.example.Book.Project.repositories.AuthorRepositories;
import com.example.Book.Project.repositories.BookRepositories;
import com.example.Book.Project.repositories.PublisherRepositories;

@RestController
@RequestMapping("/api")
public class BookController {
	ModelMapper modelMapper = new ModelMapper();
	
	
	
	@Autowired
	BookRepositories bookRepository;
	
	@Autowired
	PublisherRepositories publisherRepository;
	
	@Autowired
	AuthorRepositories authorRepository;
	
	
	@GetMapping("/books/readAll")
	public HashMap<String,Object>getAllBookDTOMapper(){
		HashMap<String, Object>result = new HashMap<String, Object>();
		
		//Mengambil semua data dari publisher dengan menggunakan method findAll() dari repository
		ArrayList<Book>listBookEntity = (ArrayList<Book>) bookRepository.findAll();
		
		
		//Membuat sebuah array list DTO
		ArrayList<BookDTO>listBookDTO = new ArrayList<BookDTO>();
		
		//mapping semua object dari entity ke DTO menggunakan looping 
		
		for(Book book : listBookEntity) {
			//inisialisasi obj Publisher DTO 
			BookDTO bookDTO = modelMapper.map(book, BookDTO.class);

			
			listBookDTO.add(bookDTO);
			
		}
		
		
		result.put("Status", 200);
		result.put("Message", "Read all book success");
		result.put("Data", listBookDTO);
		
		return result;
	}

	
	
	
	//CREATE MAPPER
	@PostMapping("/books/creates")
	public HashMap<String, Object> createBookDTOMapper(@Valid @RequestBody BookDTO body){
		HashMap<String , Object> result = new HashMap<String, Object>();
		
		
		Book bookEntity = modelMapper.map(body, Book.class);
		//proses saving data ke database
		
		
	
		
		calculateBookPrice(body, bookEntity);
		
		bookRepository.save(bookEntity);
		body = modelMapper.map(bookEntity, BookDTO.class);
		
		body.setBookId(bookEntity.getBookId());
		
		
		//Mapping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Create new bookDTO MApper success");
		result.put("Data", body);
		
		
		return result;
	}
	
	
	public void calculateBookPrice(BookDTO bookDTO, Book bookEntity) {
		BigDecimal paperPrice = new BigDecimal(0);
		BigDecimal ratingBook = new BigDecimal(0);
		BigDecimal ratePrice = new BigDecimal(1.1);
		
		Long publisherID = bookDTO.getPublisherMap().getPublisherID(), authorId = bookDTO.getAuthorMap().getAuthorId();
		
		Publisher publisherEntity = publisherRepository.findById(publisherID).orElseThrow(()-> new ResourceNotFoundException("Publisher", "id", publisherID));
		Author authorEntity = authorRepository.findById(authorId).orElseThrow(()-> new ResourceNotFoundException("Author", "id", authorId));
		
		
		paperPrice = publisherEntity.getPaperQuality().getPaperPrice();
		ratingBook = authorEntity.getRatingAuthor().getRatePrice();
//		ArrayList<Book> listBookEntity = (ArrayList<Book>) bookRepository.findAll();
//		
//		for(Book book : listBookEntity) {
//			if((book.getPublisherMap().getPublisherID() == publisherID) 
//					&& (book.getAuthorMap().getAuthorId() == AuthorId)) {
//				paperPrice = book.getPublisherMap().getPaperQuality().getPaperPrice();
//				ratingBook = book.getAuthorMap().getRatingAuthor().getRatePrice();
//			}
//		}
		
		BigDecimal totalPrice = paperPrice.multiply(ratePrice).multiply(ratingBook);
		
		bookEntity.setPrice(totalPrice);
		
	}
	
	// UPDATE MAPPER
	
	@PutMapping("/books/update/{id}")
	public HashMap<String, Object> updateBookDTOMapper(@PathVariable(value = "id") Long bookId, @Valid @RequestBody BookDTO body){
		HashMap<String , Object> result = new HashMap<String, Object>();
//		int totalPrice;
		
		Book bookEntity = bookRepository.findById(bookId).orElseThrow(()-> new ResourceNotFoundException("Book", "id", bookId));
		
		bookEntity = modelMapper.map(body, Book.class);
		
		bookEntity.setPrice(body.getPrice());		
		
		bookEntity.setBookId(bookId);
		
		bookRepository.save(bookEntity);
		
		body = modelMapper.map(bookEntity, BookDTO.class);
		
		
		//Mapping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Update new bookDTO Mapp success");
		result.put("Data", body);
		
		
		return result;
	}
	
	


	@DeleteMapping("/books/delete/{id}")
	public HashMap<String, Object>deleteBookDTOMapper(@PathVariable(value = "id")Long bookId,  BookDTO body){
		HashMap<String , Object> result = new HashMap<String, Object>();
		
		
		Book bookEntity = bookRepository.findById(bookId)
				.orElseThrow(()-> new ResourceNotFoundException("Book", "id", bookId));
		
		bookEntity = modelMapper.map(body, Book.class);
		bookEntity.setBookId(bookId);
		bookRepository.delete(bookEntity);
		
		
		result.put("Status", 200);
		result.put("Message", "Delete bookDTO Mapp success");
		result.put("Data", "[]");
		
		return result;
		
	}
	
	@PutMapping("/books/updateAutoCal/{id}")
	public HashMap<String, Object> updateBookPriceAuto(@PathVariable(value = "id") Long bookId, @Valid @RequestBody BookDTO body) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		

		Book book = bookRepository.findById(bookId).orElseThrow(() -> new ResourceNotFoundException("Book", "id", bookId));
		Book bookEntity = modelMapper.map(body, Book.class);
//		book = modelMapper.map(body, Book.class);
		book.setBookId(bookId);
		
		calculateBookPrice(body, bookEntity);
		
	    bookRepository.save(book);
	    body = modelMapper.map(book, BookDTO.class);
	    
		result.put("Status", 200);
		result.put("Message", "Update book  with id: "+ bookId +" success");
		result.put("Data", body);
		
		return result;
	}
}

//@PutMapping("/book/updateDTO/{id}")
//public HashMap<String, Object> updateBookDTO(@PathVariable(value = "id") Long bookId, @Valid @RequestBody BookDTO body){
//	HashMap<String , Object> result = new HashMap<String, Object>();
//	
//	
//	Book bookEntity = bookRepository.findById(bookId).orElseThrow(()-> new ResourceNotFoundException("Book", "id", bookId));
//	
//	
//	Author authorEntity = new Author();
//	
//	Publisher publisherEntity = new Publisher();
//	
//	authorEntity.setAuthorId(body.getAuthorMap().getAuthorId());
//	
//	publisherEntity.setPublisherID(body.getPublisherMap().getPublisherID());
//	
//	bookEntity.setPrice(body.getPrice());
//	bookEntity.setReleaseDate(body.getReleaseDate());
//	bookEntity.setTitle(body.getTitle());
//	bookEntity.setAuthorMap(authorEntity);
//	bookEntity.setPublisherMap(publisherEntity);
//	
//	//proses saving data ke database
//	bookRepository.save(bookEntity);
//	
//	body.setBookId(bookEntity.getBookId());
//	
//	
//	//Mapping result untuk response API
//	result.put("Status", 200);
//	result.put("Message", "update book success");
//	result.put("Data", body);
//	
//	
//	
//	return result;
//}
//

//@DeleteMapping("/book/deleteDTO/{id}")
//public HashMap<String, Object>deletePublisherDTO(@PathVariable(value = "id")Long bookId, BookDTO body){
//	HashMap<String , Object> result = new HashMap<String, Object>();
//	
//	
//	Book bookEntity = bookRepository.findById(bookId)
//			.orElseThrow(()-> new ResourceNotFoundException("Book", "id", bookId));
//	
//	Author authorEntity = new Author();
//	
//	Publisher publisherEntity = new Publisher();
//	
//	authorEntity.setAuthorId(body.getAuthorMap().getAuthorId());
//	
//	publisherEntity.setPublisherID(body.getPublisherMap().getPublisherID());
//	
//	bookEntity.setPrice(body.getPrice());
//	bookEntity.setReleaseDate(body.getReleaseDate());
//	bookEntity.setTitle(body.getTitle());
//	bookEntity.setAuthorMap(authorEntity);
//	bookEntity.setPublisherMap(publisherEntity);
//	
//	//proses saving data ke database
//	
//	bookRepository.delete(bookEntity);
//	
//	
//	
//	result.put("Status", 200);
//	result.put("Message", "Delete book success");
//	result.put("Data", body);
//	
//	return result;
//	
//}

//@GetMapping("/book2/readAll")
//public List<Book>getAllBook(){
//	return bookRepository.findAll();
//}
//
//@GetMapping("/book2/readAllDTO")
//public HashMap<String,Object>getAllData(){
//	HashMap<String, Object>result = new HashMap<String, Object>();
//	
//	//Mengambil semua data dari publisher dengan menggunakan method findAll() dari repository
//	ArrayList<Book>listBookEntity = (ArrayList<Book>) bookRepository.findAll();
//	
//	
//	//Membuat sebuah array list DTO
//	ArrayList<BookDTO>listBookDTO = new ArrayList<BookDTO>();
//	
//	//mapping semua object dari entity ke DTO menggunakan looping 
//	
//	for(Book books : listBookEntity) {
//		//inisialisasi obj Publisher DTO 
//		BookDTO bookDTO = new BookDTO();
//		//inisialisasi obj Paper DTO
//		AuthorDTO authorDTO = new AuthorDTO();
//		
//		PublisherDTO publisherDTO = new PublisherDTO();
//		
//		PaperDTO paperDTO = new PaperDTO();
//		
//		//Mapping nilai dari entity ke DTO
//		paperDTO.setPaperid(books.getPublisherMap().getPaperQuality().getPaperid());
//		paperDTO.setPaperPrice(books.getPublisherMap().getPaperQuality().getPaperPrice());
//		paperDTO.setQualityName(books.getPublisherMap().getPaperQuality().getQualityName());
//		
//		authorDTO.setAuthorId(books.getAuthorMap().getAuthorId());
//		authorDTO.setAge(books.getAuthorMap().getAge());
//		authorDTO.setCountry(books.getAuthorMap().getCountry());
//		authorDTO.setFirstName(books.getAuthorMap().getFirstName());
//		authorDTO.setLastName(books.getAuthorMap().getLastName());
//		authorDTO.setGender(books.getAuthorMap().getGender());
//		authorDTO.setRating(books.getAuthorMap().getRating());
//		
//		publisherDTO.setPublisherID(books.getPublisherMap().getPublisherID());
//		publisherDTO.setCompanyName(books.getPublisherMap().getCompanyName());
//		publisherDTO.setCountry(books.getPublisherMap().getCountry());
//		publisherDTO.setPaperQuality(paperDTO);
//		
//		
//		bookDTO.setBookId(books.getBookId());
//		bookDTO.setPrice(books.getPrice());
//		bookDTO.setReleaseDate(books.getReleaseDate());
//		bookDTO.setTitle(books.getTitle());
//		bookDTO.setAuthorMap(authorDTO);
//		bookDTO.setPublisherMap(publisherDTO);
//		
//		
//		listBookDTO.add(bookDTO);
//		
//	}
//	
//	
//	result.put("Status", 200);
//	result.put("Message", "Read all publisherDTO success");
//	result.put("Data", listBookDTO);
//	
//	return result;
//}

//@PostMapping("/book2/creates")
//public Book createBook(@Valid @RequestBody Book book){
//	return bookRepository.save(book);
//}
//
//@PostMapping("/book2/createsDTO")
//public HashMap<String, Object> createBookDTO(@Valid @RequestBody BookDTO body){
//	HashMap<String , Object> result = new HashMap<String, Object>();
//	
//	//buat sebuah object entity
//	Book bookEntity =new Book();
//	
//	Author authorEntity = new Author();
//	
//	Publisher publisherEntity = new Publisher();
//	
//	authorEntity.setAuthorId(body.getAuthorMap().getAuthorId());
//	
//	publisherEntity.setPublisherID(body.getPublisherMap().getPublisherID());
//	
//	bookEntity.setPrice(body.getPrice());
//	bookEntity.setReleaseDate(body.getReleaseDate());
//	bookEntity.setTitle(body.getTitle());
//	bookEntity.setAuthorMap(authorEntity);
//	bookEntity.setPublisherMap(publisherEntity);
//	
//	//proses saving data ke database
//	bookRepository.save(bookEntity);
//	
//	body.setBookId(bookEntity.getBookId());
//	
//	
//	//Mapping result untuk response API
//	result.put("Status", 200);
//	result.put("Message", "Create new book success");
//	result.put("Data", body);
//	
//	
//	return result;
//}
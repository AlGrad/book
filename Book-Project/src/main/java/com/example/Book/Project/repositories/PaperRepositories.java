package com.example.Book.Project.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.Book.Project.models.Paper;


@Repository
public interface PaperRepositories extends JpaRepository<Paper, Long>{

}

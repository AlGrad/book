package com.example.Book.Project.DTO;

import com.example.Book.Project.models.Paper;

public class PublisherDTO {
	private Long publisherID;
	private String companyName;
	private String country;
	private PaperDTO paperQuality;
	
	

	public PublisherDTO() {
		super();
	}
	

	public PublisherDTO(Long publisherID, String companyName, String country, PaperDTO paperQuality) {
		super();
		this.publisherID = publisherID;
		this.companyName = companyName;
		this.country = country;
		this.paperQuality = paperQuality;
	}


	public Long getPublisherID() {
		return publisherID;
	}

	public void setPublisherID(Long publisherID) {
		this.publisherID = publisherID;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}


	public PaperDTO getPaperQuality() {
		return paperQuality;
	}


	public void setPaperQuality(PaperDTO paperQuality) {
		this.paperQuality = paperQuality;
	}
	
	
	
}

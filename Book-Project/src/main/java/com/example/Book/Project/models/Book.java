package com.example.Book.Project.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity(name = "Book")
@Table(name = "Books")
//@EntityListeners(AuditingEntityListener.class)
//@JsonIgnoreProperties(value = {"createdAt", "updatedAt"},
//	allowGetters = true)
public class Book implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "bookID", nullable = false, unique = true)
	private Long bookId;
	
	@Column(name = "title", nullable = false)
	private String title;
	
	@JsonFormat(pattern = "yyyy-mm-dd")
	@Column(name = "releaseDate", nullable = false)
	private Date releaseDate;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "authorID")
	private Author authorMap;
	
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "publisherID")
	private Publisher publisherMap;
	
	@Column(name = "price", nullable = true)
	private BigDecimal price;
	 
	 
	public Book() {
		super();
	}
	
	public Long getBookId() {
		return bookId;
	}
	public void setBookId(Long bookId) {
		this.bookId = bookId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Date getReleaseDate() {
		return releaseDate;
	}
	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}
	
	
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	
	public Author getAuthorMap() {
		return authorMap;
	}

	public void setAuthorMap(Author authorMap) {
		this.authorMap = authorMap;
		authorMap.getBooks().add(this);
	}

	public Publisher getPublisherMap() {
		return publisherMap;
	}

	public void setPublisherMap(Publisher publisherMap) {
		this.publisherMap = publisherMap;
		publisherMap.getBooks().add(this);
	}
	 
	

}

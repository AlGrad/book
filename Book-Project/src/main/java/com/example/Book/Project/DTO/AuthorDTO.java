package com.example.Book.Project.DTO;

import com.example.Book.Project.models.Rating;

public class AuthorDTO {
	private Long authorId;
	private String firstName;
	private String lastName;
	private String gender;
	private int age;
	private String country;
	private RatingDTO ratingAuthor;
	
	
	public AuthorDTO() {
		super();
	}

	
	
	public AuthorDTO(Long authorId, String firstName, String lastName, String gender, int age, String country,
			RatingDTO ratingAuthor) {
		super();
		this.authorId = authorId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.gender = gender;
		this.age = age;
		this.country = country;
		this.ratingAuthor = ratingAuthor;
	}



	public Long getAuthorId() {
		return authorId;
	}


	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}


	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public String getGender() {
		return gender;
	}


	public void setGender(String gender) {
		this.gender = gender;
	}


	public int getAge() {
		return age;
	}


	public void setAge(int age) {
		this.age = age;
	}


	public String getCountry() {
		return country;
	}


	public void setCountry(String country) {
		this.country = country;
	}



	public RatingDTO getRatingAuthor() {
		return ratingAuthor;
	}



	public void setRatingAuthor(RatingDTO ratingAuthor) {
		this.ratingAuthor = ratingAuthor;
	}

	
	
	
	

}

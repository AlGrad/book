package com.example.Book.Project.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.Book.Project.models.Rating;


@Repository
public interface RatingRepositories extends JpaRepository<Rating, Long>{

}

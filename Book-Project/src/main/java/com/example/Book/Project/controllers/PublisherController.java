package com.example.Book.Project.controllers;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.Book.Project.DTO.PaperDTO;
import com.example.Book.Project.DTO.PublisherDTO;
import com.example.Book.Project.exceptions.ResourceNotFoundException;
import com.example.Book.Project.models.Paper;
import com.example.Book.Project.models.Publisher;
import com.example.Book.Project.repositories.PaperRepositories;
import com.example.Book.Project.repositories.PublisherRepositories;

@RestController
@RequestMapping("/api")
public class PublisherController {
	ModelMapper modelMapper = new ModelMapper();
	
	
	@Autowired
	PublisherRepositories publisherRepository;
	

	@GetMapping("/publishers/readAllDTOMapper")
	public HashMap<String,Object>getAllPublisherDTOMapper(){
		HashMap<String, Object>result = new HashMap<String, Object>();
		
		//Mengambil semua data dari publisher dengan menggunakan method findAll() dari repository
		ArrayList<Publisher>listPublisherEntity = (ArrayList<Publisher>) publisherRepository.findAll();
		
		
		//Membuat sebuah array list DTO
		ArrayList<PublisherDTO>listPublisherDTO = new ArrayList<PublisherDTO>();
		
		//mapping semua object dari entity ke DTO menggunakan looping 
		
		for(Publisher publisher : listPublisherEntity) {
			//inisialisasi obj Publisher DTO 
			PublisherDTO publisherDTO = modelMapper.map(publisher, PublisherDTO.class);

			
			listPublisherDTO.add(publisherDTO);
			
		}
		
		
		result.put("Status", 200);
		result.put("Message", "Read All publisherDTO Mappr success");
		result.put("Data", listPublisherDTO);
		
		return result;
	}
	
	

	@PostMapping("/publishers/creates")
	public HashMap<String, Object> createPublisherDTOMapper(@Valid @RequestBody PublisherDTO body){
		HashMap<String , Object> result = new HashMap<String, Object>();
		
	
		
		Publisher publisherEntity = modelMapper.map(body, Publisher.class);
		//proses saving data ke database
	
		
		publisherRepository.save(publisherEntity);
		
		body.setPublisherID(publisherEntity.getPublisherID());
		
		
		//Mapping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Create new publisherDTO Mapp success");
		result.put("Data", body);
		
		
		return result;
	}

	@PutMapping("/publishers/update/{id}")
	public HashMap<String, Object> updatePublisherDTOMapper(@PathVariable(value = "id") Long publisherID, @Valid @RequestBody PublisherDTO body){
		HashMap<String , Object> result = new HashMap<String, Object>();
		
		Publisher publisherEntity = publisherRepository.findById(publisherID).orElseThrow(()-> new ResourceNotFoundException("Publisher", "id", publisherID));
		
		
		publisherEntity = modelMapper.map(body, Publisher.class);
		
		publisherEntity.setPublisherID(publisherID);
	
		
		publisherRepository.save(publisherEntity);
		
		body = modelMapper.map(publisherEntity, PublisherDTO.class);
		
		
		//Mapping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Update publisher DTO Mapp success");
		result.put("Data", body);
		
		
		return result;
	}
	
	

	
	@DeleteMapping("/publishers/delete/{id}")
	public HashMap<String, Object>deletePublisherDTOMapper(@PathVariable(value = "id")Long publisherID,  PublisherDTO body){
		HashMap<String , Object> result = new HashMap<String, Object>();
		
		
		Publisher publisherEntity = publisherRepository.findById(publisherID)
				.orElseThrow(()-> new ResourceNotFoundException("Publisher", "id", publisherID));
		
		publisherEntity = modelMapper.map(body, Publisher.class);
		publisherEntity.setPublisherID(publisherID);
		
		publisherRepository.delete(publisherEntity);
		
		
		result.put("Status", 200);
		result.put("Message", "Delete paper success");
		result.put("Data", "[]");
		
		return result;
		
	}

}

//
//@PostMapping("/publishers/creates")
//public Publisher createPublisher(@Valid @RequestBody Publisher publisher){
//	return publisherRepository.save(publisher);
//}
//
//@PostMapping("/publishers/createsDTO")
//public HashMap<String, Object> createPublisherDTO(@Valid @RequestBody PublisherDTO body){
//	HashMap<String , Object> result = new HashMap<String, Object>();
//	
//	//buat sebuah object publisher entity
//	Publisher publisherEntity =new Publisher();
//	
//	//inisialisasi object paper entity
//	Paper paperEntity = new Paper();
//	
//	//mapping value dari paperDTO ke paper entity
//	paperEntity.setPaperid(body.getPaperQuality().getPaperid());
//	
//	//mapping value dari publisher DTO to entity
//	publisherEntity.setCompanyName(body.getCompanyName());
//	publisherEntity.setCountry(body.getCountry());
//	publisherEntity.setPaperQuality(paperEntity);
//	
//	
//	//proses saving data ke database
//	publisherRepository.save(publisherEntity);
//	
//	body.setPublisherID(publisherEntity.getPublisherID());
//	
//	//Mapping result untuk response API
//	result.put("Status", 200);
//	result.put("Message", "Create new publisher success");
//	result.put("Data", body);
//	
//	
//	return result;
//}
//

//
//@PutMapping("/publishers/update/{id}")
//public Publisher updatePublisher(@PathVariable(value = "id") Long publisherID, @Valid @RequestBody Publisher publisherDetails) {
//	Publisher publisher = publisherRepository.findById(publisherID).orElseThrow(()-> new ResourceNotFoundException("Publisher", "id", publisherID));
//	
//	publisher.setCompanyName(publisherDetails.getCompanyName());
//	publisher.setCountry(publisherDetails.getCountry());
//
//	
//	Publisher updatePublisher = publisherRepository.save(publisher);
//	return updatePublisher;
//}
//
//
//@PutMapping("/publishers/updateDTO/{id}")
//public HashMap<String, Object> updatePublisherDTO(@PathVariable(value = "id") Long publisherID, @Valid @RequestBody PublisherDTO body){
//	HashMap<String , Object> result = new HashMap<String, Object>();
//	
//	
//	Publisher publisherEntity = publisherRepository.findById(publisherID).orElseThrow(()-> new ResourceNotFoundException("Publisher", "id", publisherID));
//	
//	Paper paperEntity = new Paper();
//
//	//buat sebuah object paper entity
//	paperEntity.setPaperid(body.getPaperQuality().getPaperid());
//	
//	//mapping value dari publisher DTO to entity
//	publisherEntity.setCompanyName(body.getCompanyName());
//	publisherEntity.setCountry(body.getCountry());
//	publisherEntity.setPaperQuality(paperEntity);
//	
//	
//	//proses saving data ke database
//	publisherRepository.save(publisherEntity);
//	
//	body.setPublisherID(publisherEntity.getPublisherID());
//	
//	//Mapping result untuk response API
//	result.put("Status", 200);
//	result.put("Message", "update publisher success");
//	result.put("Data", body);
//	
//	
//	
//	return result;
//}
//

//@GetMapping("/publishers/readAll")
//public List<Publisher>getAllPublisher(){
//	return publisherRepository.findAll();
//}

//
//@DeleteMapping("/publishers/delete/{id}")
//public ResponseEntity<Publisher>deletePublisher(@PathVariable(value = "id")Long publisherID){
//	Publisher publisher = publisherRepository.findById(publisherID).orElseThrow(()-> new ResourceNotFoundException("Publisher", "id", publisherID));
//	
//	publisherRepository.delete(publisher);
//	
//	return ResponseEntity.ok().build();
//	
//}
//
//@DeleteMapping("/publishers/deleteDTO/{id}")
//public HashMap<String, Object>deletePublisherDTO(@PathVariable(value = "id")Long publisherID, PublisherDTO body){
//	HashMap<String , Object> result = new HashMap<String, Object>();
//	
//	
//	Publisher publisherEntity = publisherRepository.findById(publisherID)
//			.orElseThrow(()-> new ResourceNotFoundException("Publisher", "id", publisherID));
//	Paper paperEntity = new Paper();
//	
//	//buat sebuah object paper entity
//	paperEntity.setPaperid(body.getPaperQuality().getPaperid());
//	
//	//mapping value dari publisher DTO to entity
//	publisherEntity.setCompanyName(body.getCompanyName());
//	publisherEntity.setCountry(body.getCountry());
//	publisherEntity.setPaperQuality(paperEntity);
//	
//	
//	publisherRepository.delete(publisherEntity);
//	
//	
//	
//	result.put("Status", 200);
//	result.put("Message", "Delete publisher success");
//	result.put("Data", body);
//	
//	return result;
//	
//}




//@GetMapping("/publishers/readAllDTO")
//public HashMap<String,Object>getAllData(){
//	HashMap<String, Object>result = new HashMap<String, Object>();
//	
//	//Mengambil semua data dari publisher dengan menggunakan method findAll() dari repository
//	ArrayList<Publisher>listPublisherEntity = (ArrayList<Publisher>) publisherRepository.findAll();
//	
//	
//	//Membuat sebuah array list DTO
//	ArrayList<PublisherDTO>listPublisherDTO = new ArrayList<PublisherDTO>();
//	
//	//mapping semua object dari entity ke DTO menggunakan looping 
//	
//	for(Publisher publisher : listPublisherEntity) {
//		//inisialisasi obj Publisher DTO 
//		PublisherDTO publisherDTO = new PublisherDTO();
//		//inisialisasi obj Paper DTO
//		PaperDTO paperDTO = new PaperDTO();
//		
//		//Mapping nilai dari entity ke DTO
//		paperDTO.setPaperid(publisher.getPaperQuality().getPaperid());
//		paperDTO.setPaperPrice(publisher.getPaperQuality().getPaperPrice());
//		paperDTO.setQualityName(publisher.getPaperQuality().getQualityName());
//		
//		//Mapping nilai dari entity publisher ke DTO
//		publisherDTO.setCompanyName(publisher.getCompanyName());
//		publisherDTO.setCountry(publisher.getCountry());
//		publisherDTO.setPublisherID(publisher.getPublisherID());
//		publisherDTO.setPaperQuality(paperDTO);
//		
//		listPublisherDTO.add(publisherDTO);
//		
//	}
//	
//	
//	result.put("Status", 200);
//	result.put("Message", "Read all publisherDTO success");
//	result.put("Data", listPublisherDTO);
//	
//	return result;
//}






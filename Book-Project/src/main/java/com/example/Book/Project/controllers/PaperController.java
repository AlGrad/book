package com.example.Book.Project.controllers;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.Book.Project.DTO.PaperDTO;
import com.example.Book.Project.DTO.PublisherDTO;
import com.example.Book.Project.exceptions.ResourceNotFoundException;
import com.example.Book.Project.models.Paper;
import com.example.Book.Project.models.Publisher;
import com.example.Book.Project.repositories.PaperRepositories;


@RestController
@RequestMapping("/api")
public class PaperController {

	ModelMapper modelMapper = new ModelMapper();

	
	
	@Autowired
	PaperRepositories paperRepository;
	
	@GetMapping("/paper/readAll")
	public HashMap<String,Object>getAllPaperDTOMapper(){
		HashMap<String, Object>result = new HashMap<String, Object>();
		
		//Mengambil semua data dari publisher dengan menggunakan method findAll() dari repository
		ArrayList<Paper>listPaperEntity = (ArrayList<Paper>) paperRepository.findAll();
		
		
		//Membuat sebuah array list DTO
		ArrayList<PaperDTO>listPaperDTO = new ArrayList<PaperDTO>();
		
		//mapping semua object dari entity ke DTO menggunakan looping 
		
		for(Paper paper : listPaperEntity) {
			//inisialisasi obj Publisher DTO 
			PaperDTO paperDTO = modelMapper.map(paper, PaperDTO.class);

			
			listPaperDTO.add(paperDTO);
			
		}
		
		
		result.put("Status", 200);
		result.put("Message", "Read All paperDTO Mappr success");
		result.put("Data", listPaperDTO);
		
		return result;
	}

	
	@PostMapping("/paper/creates")
	public HashMap<String, Object> createPaperDTO(@Valid @RequestBody PaperDTO body){
		HashMap<String , Object> result = new HashMap<String, Object>();
		
	
		
		Paper paperEntity = modelMapper.map(body, Paper.class);
		//proses saving data ke database
	
		
		paperRepository.save(paperEntity);
		
		body.setPaperid(paperEntity.getPaperid());
		
		
		//Mapping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Create new paper success");
		result.put("Data", body);
		
		
		return result;
	}
	

	
	//UPDATE MAPPER
	
	@PutMapping("/paper/update/{id}")
	public HashMap<String, Object> updatePaperDTOMapper(@PathVariable(value = "id") Long paperid, @Valid @RequestBody PaperDTO body){
		HashMap<String , Object> result = new HashMap<String, Object>();
		
		Paper paperEntity = paperRepository.findById(paperid).orElseThrow(()-> new ResourceNotFoundException("PaperQuality", "id", paperid));
		
		
		paperEntity = modelMapper.map(body, Paper.class);
		
		paperEntity.setPaperid(paperid);
	
		
		paperRepository.save(paperEntity);
		
		body = modelMapper.map(paperEntity, PaperDTO.class);
		
		
		//Mapping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Update new paper success");
		result.put("Data", body);
		
		
		return result;
	}
	

	
	@DeleteMapping("/paper/delete/{id}")
	public HashMap<String, Object>deletePaperDTOMapper(@PathVariable(value = "id")Long paperid,  PaperDTO body){
		HashMap<String , Object> result = new HashMap<String, Object>();
		
		
		Paper paperEntity = paperRepository.findById(paperid)
				.orElseThrow(()-> new ResourceNotFoundException("Paper", "id", paperid));
		
		paperEntity = modelMapper.map(body, Paper.class);
		paperEntity.setPaperid(paperid);
		
		paperRepository.delete(paperEntity);
		
		
		result.put("Status", 200);
		result.put("Message", "Delete paper success");
		result.put("Data", "[]");
		
		return result;
		
	}
	

}



//@PostMapping("/paper/createsDTO")
//public HashMap<String, Object> createPaperDTOMapper(@Valid @RequestBody PaperDTO body){
//	HashMap<String , Object> result = new HashMap<String, Object>();
//	
//	//buat sebuah object paper entity
//	Paper paperEntity =new Paper();
//	paperEntity.setPaperPrice(body.getPaperPrice());
//	paperEntity.setQualityName(body.getQualityName());
//	//proses saving data ke database
//	paperRepository.save(paperEntity);
//	
//	body.setPaperid(paperEntity.getPaperid());
//	
//	
//	//Mapping result untuk response API
//	result.put("Status", 200);
//	result.put("Message", "Create new paper success");
//	result.put("Data", body);
//	
//	
//	return result;
//}


//
//@PutMapping("/paper/update/{id}")
//public Paper updatePaper(@PathVariable(value = "id") Long paperID, @Valid @RequestBody Paper paperDetails) {
//	Paper paper = paperRepository.findById(paperID).orElseThrow(()-> new ResourceNotFoundException("PaperQuality", "id", paperID));
//	
//	paper.setPaperPrice(paperDetails.getPaperPrice());
//	paper.setQualityName(paperDetails.getQualityName());
//	
//	
//	Paper updatePaper = paperRepository.save(paper);
//	return updatePaper;
//}
//
//
//@PutMapping("/paper/updateDTO/{id}")
//public HashMap<String, Object> updatePaperDTO(@PathVariable(value = "id") Long paperid, @Valid @RequestBody PaperDTO body){
//	HashMap<String , Object> result = new HashMap<String, Object>();
//	
//	Paper paperEntity = paperRepository.findById(paperid).orElseThrow(()-> new ResourceNotFoundException("PaperQuality", "id", paperid));
//	
//	//buat sebuah object paper entity
//	
//	paperEntity.setPaperPrice(body.getPaperPrice());
//	paperEntity.setQualityName(body.getQualityName());
//	//proses saving data ke database
//	paperRepository.save(paperEntity);
//	
//	paperEntity.setPaperid(body.getPaperid());
//	
//	
//	//Mapping result untuk response API
//	result.put("Status", 200);
//	result.put("Message", "Update paper success");
//	result.put("Data", body);
//	
//	
//	return result;
//}

//@GetMapping("/paper/readAll")
//public List<Paper>getAllPaper(){
//	return paperRepository.findAll();
//}
//
//@GetMapping("/paper/readAllDTO")
//public HashMap<String,Object>getAllPaperDTO(){
//	HashMap<String, Object>result = new HashMap<String, Object>();
//	
//	//Mengambil semua data dari publisher dengan menggunakan method findAll() dari repository
//	ArrayList<Paper>listPaperEntity = (ArrayList<Paper>) paperRepository.findAll();
//	
//	
//	//Membuat sebuah array list DTO
//	ArrayList<PaperDTO>listPaperDTO = new ArrayList<PaperDTO>();
//	
//	//mapping semua object dari entity ke DTO menggunakan looping 
//	
//	for(Paper paper : listPaperEntity) {
//		//inisialisasi obj Publisher DTO 
//		PaperDTO paperDTO = new PaperDTO();
//
//		
//		//Mapping nilai dari entity ke DTO
//		paperDTO.setPaperid(paper.getPaperid());
//		paperDTO.setPaperPrice(paper.getPaperPrice());
//		paperDTO.setQualityName(paper.getQualityName());
//		
//
//		
//		listPaperDTO.add(paperDTO);
//		
//	}
//	
//	
//	result.put("Status", 200);
//	result.put("Message", "Read All paperDTO success");
//	result.put("Data", listPaperDTO);
//	
//	return result;
//}
//
//

//@PostMapping("/paper/creates")
//public Paper createPaper(@Valid @RequestBody Paper paper){
//	return paperRepository.save(paper);
//}
//
//@DeleteMapping("/paper/delete/{id}")
//public ResponseEntity<Paper>deletePaper(@PathVariable(value = "id")Long paperid){
//	Paper paper = paperRepository.findById(paperid).orElseThrow(()-> new ResourceNotFoundException("PaperQuality", "id", paperid));
//	
//	paperRepository.delete(paper);
//	
//	return ResponseEntity.ok().build();
//	
//}
//
//@DeleteMapping("/paper/deleteDTO/{id}")
//public HashMap<String, Object>deletePaperDTO(@PathVariable(value = "id")Long paperid,  PaperDTO body){
//	HashMap<String , Object> result = new HashMap<String, Object>();
//	
//	
//	Paper paperEntity = paperRepository.findById(paperid)
//			.orElseThrow(()-> new ResourceNotFoundException("Paper", "id", paperid));
//	
//	
//	paperEntity.setPaperPrice(body.getPaperPrice());
//	paperEntity.setQualityName(body.getQualityName());
//	
//	
//	paperRepository.delete(paperEntity);
//	
//	
//	
//	result.put("Status", 200);
//	result.put("Message", "Delete paper success");
//	result.put("Data", body);
//	
//	return result;
//	
//}